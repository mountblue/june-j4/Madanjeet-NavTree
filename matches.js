const MongoClient = require("mongodb").MongoClient;
let url = "mongodb://127.0.0.1:27017";

function getEachSeason() {
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, conn) {
            if (err) {
                console.log(err.message);
                reject(err);
            } else {
                let ipldb = conn.db("ipldb");
                let matchesCol = ipldb.collection("matchesCol");
                matchesCol.distinct("season", function (err, docs) {
                    if (err) {
                        return (err);
                    } else {
                        // console.log(docs);
                        resolve(docs)
                    }
                });
            }
            // conn.close();
        })
    })
}

function getEachTeam(year) {
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, conn) {
            if (err) {
                console.log(err.message);
                reject(err);
            } else {
                let ipldb = conn.db("ipldb");
                let matchesCol = ipldb.collection("matchesCol");
                matchesCol.distinct("team1",{"season":Number(year)}, function (err, docs) {
                    if (err) {
                        return (err);
                    } else {
                        console.log(docs);
                        resolve(docs)
                    }
                });
            }
            // conn.close();
        })
    })
}

function getEachPlayer(season,teams) {
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, conn) {
            if (err) {
                console.log(err.message);
                reject(err);
            } else {
                let ipldb = conn.db("ipldb");
                let matchesCol = ipldb.collection("matchesCol");
                matchesCol.aggregate(
                    [{
                            "$match": {
                                season: Number(season),//2017,
                                team1:teams//"Sunrisers Hyderabad"
                            }
                        },
                        {
                            "$lookup": {
                                from: "deliveriesCol",
                                localField: "id",
                                foreignField: "match_id",
                                as: "id"
                            }
                        },
                        {
                            "$unwind": "$id"
                        },
                        {
                           $project:
                           {
                               _id:0,
                                team:"$id.batting_team",
                                player:"$id.batsman"
                           }
                        },
                        {
                            $match:
                            {
                                team:teams//"Sunrisers Hyderabad"
                            }
                        }
                    ]).toArray(function (err, data) {
                    if (err) {
                        console.log(err.message);
                        reject(err);
                    } else {
                        let newData=[];
                        data.forEach(element => {
                            if(!newData.includes(element.player))
                            newData.push(element.player);
                        });
                        console.log(newData);
                        resolve(newData);
                    }
                })
            }
            // conn.close();
        })
    })
}
// getEachPlayer(2015,"Sunrisers Hyderabad");
// getEachTeam();
// getEachSeason();
module.exports = {
    getEachSeason : getEachSeason,
    getEachTeam : getEachTeam,
    getEachPlayer:getEachPlayer
}

// const mongoose=require("mongoose");element

// let matchesSchema= mongoose.Schema({
//     season:{
//         type:Number,
//         required:true
//     }
// })

// var Matches = module.exports=mongoose.model('Matches',matchesSchema)
// module.exports.getMatchesSeason = function(callback){
//     Matches.distinct('season',callback)
// }