const express = require('express')
const app = express();

const Matches = require('./matches.js')

app.get('/',function(req,res){
    res.sendFile(__dirname+'/index.html')
})

app.get('/seasons',function(req,res,next){
    Matches.getEachSeason().then(function(data){
        res.json(data)
    })
})

app.get('/seasons/:season',function(req,res,next){
    const season = req.params.season;
    Matches.getEachTeam(season).then(function(data){
        res.json(data)
    })
})

app.get('/seasons/:season/teams/:team',function(req,res,next){
    const season= req.params.season;
    const team = req.params.team;
    Matches.getEachPlayer(season,team).then(function(data){
        res.json(data)
    })
})

app.listen(3000);
console.log("server running on 3000");